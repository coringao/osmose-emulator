
           _______                  _______
          |       |.-----.--------.|       |.-----.-----.
          |   -   ||__ --|        ||   -   ||__ --|  -__|
          |_______||_____|__|__|__||_______||_____|_____|
           _______                  __         __
          |    ___|.--------.--.--.|  |.---.-.|  |_.-----.----.
          |    ___||        |  |  ||  ||  _  ||   _|  _  |   _|
          |_______||__|__|__|_____||__||___._||____|_____|__|
        Public release, re-written and continued by Carlos Donizete Froes.
        Original copyright holder: Bruno Vedder.

### What is Osmose Emulator?

In brief it's a Sega Master System/Game Gear emulator encapsulated into C++ classes.

Many thanks to Vedder Bruno, the original author of Osmose Emulator.

*What's new:*
-------------

Osmose Emulator now has a clean graphical user interface based on Qt version 5.x,
a GUI library which is fast enough to refresh the display at 60 Hz.

	- Switch sound system to use ALSA.
	- New GUI based on Qt5.
	- Rendering/Key mapping adapted to QtOpenGL.
	- Configuration system rewritten.
	- Functional on i386, x86-64 and arm architectures.

*Dependencies for Debian/Ubuntu/GNU/Linux:*
-------------------------------------------

    # apt install freeglut3-dev g++ libasound2-dev libqt5opengl5-dev libgl1-mesa-dev make mesa-common-dev qt5-qmake qt5-default qtbase5-dev qtchooser zlib1g-dev

*Compilation:*
--------------

To build osmose emulator, run the following commands from the source directory:

*Build:*

    $ qmake && make

*Clean the build artifacts:*

    $ make clean && make distclean

*Features:*
-----------

- SMS: Good compatibility. At this stage, the emulator can run **96%** of
commercial games and public demos, except games that rely on
codemaster mapper, which works but does not have proper video mode emulated.
- Game Gear: Good compatibility. At this stage, the emulator can run
**98%** of game gear ROMS.
- SN76489 Sound is supported.
- Support for ".sms"  and ".gg" format.
- Video filters: bilinear or nearest neighbour (default).
- Pad(keyboard or joystick mapped) emulation.
- PAL/NTSC Timing.
- Japanese/Export console.
- In game Screenshots, GFX rip, sound shot.
- Configurable keyboard configuration.
- Joystick support, configurable button assignement.
- Drag and drop your ROMS into the emulator window to run games.

**Due to the huge number of game gear/master system (around 1300) ROMs, games have not been deeply tested.**

* This file was officially downloaded from: http://bcz.asterope.fr (The original developer's site was deactivated at the end of 2018).
* Continuation of the project on: https://gitlab.com/coringao/osmose-emulator

**License**
-----------

> **Osmose Emulator** is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.

- Copyright (c) 2001-2011 **Bruno Vedder**
- Copyright (c) 2016-2020 Work continued by **Carlos Donizete Froes [a.k.a coringao]**
