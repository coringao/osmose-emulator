/*
 * Copyright holder 2001-2011 Vedder Bruno.
 * Work continued by 2016-2020 Carlos Donizete Froes [a.k.a coringao]
 *
 * This file is part of Osmose Emulator, a Sega Master System and Game Gear
 * software emulator.
 *
 * Osmose Emulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osmose Emulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osmose Emulator. If not, see <http://www.gnu.org/licenses/>.
 *
 * Many thanks to Vedder Bruno, the original author of Osmose Emulator.
 *
 */

#include "WhiteNoiseEmulationThread.h"

/**
 * Constructor.
 */
WhiteNoiseEmulationThread::WhiteNoiseEmulationThread(QGLImage *qglimage) : EmulationThread(qglimage)
{
	setRefreshFrequency(25.0f);
}

/**
 * Destructor.
 */
WhiteNoiseEmulationThread::~WhiteNoiseEmulationThread()
{
}

/**
 * This method perform one frame emulation stuff. The videoBuffer must
 * be updated inside this method.
 */
void WhiteNoiseEmulationThread::emulateOneFrame()
{
	unsigned int col = 0;
	for (unsigned int i = 0; i < 256 * 192; i++)
	{
		int hzd = rand() & 0xFF;
		if (hzd > 0xF0)
		{
			col = rand() & 0xFF;
			col = col | (col << 8) | (col << 16) | 0xFF000000;
		}
		else
		{
			col = 0xFF000000;
		}
		videoBuffer[i] = col;
	}
}
